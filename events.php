<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Sample Events Class
 *
 * @package
 * @subpackage
 * @category
 * @author
 * @website
 */
class Events_Graphit
{
    
    protected $ci;
    
    public function __construct() {
        $this->ci = & get_instance();
        Events::register('last_graph', array(
            $this,
            'get_last'
        ));
        Events::register('create_real_graph', array(
            $this,
            'create_real'
        ));
        Events::register('get_all_names_graphit', array(
            $this,
            'get_names'
        ));
        Events::register('get_names_by_name', array(
            $this,
            'get_names_by_name'
        ));
    }
    
    public function get_last() {
        $this->ci->load->model('graphit/graphit_m');
        return $this->ci->graphit_m->get_lastest();
    }
    
    public function create_real($input) {
        $this->ci->load->model('graphit/graphit_m');
        $this->ci->graphit_m->create_real($input);
    }
    
    public function get_names() {
        $this->ci->load->model('graphit/graphit_m');
        return $this->ci->graphit_m->get_names();
    }
    
    public function get_names_by_name($name) {
        $this->ci->load->model('graphit/graphit_m');
        return $this->ci->graphit_m->get_data_names_by_name($name);
    }
}

/* End of file events.php */
