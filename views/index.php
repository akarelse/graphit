<div class="graphit-container">
    <div id="graphnames">    <b><?php echo lang('graphit:graph_names') ?></b>
        <?php
        foreach ($graphnames as $value) {
        echo "<a href='" . $value['url'] . "'>" . $value['name'] . "</a>&nbsp;";
    } ?></div>
    <br/>
    <div id="averages">    <b><?php echo lang('graphit:average_of') ?></b>
        <?php
        foreach ($averages as $value) {
        echo "<a href='" . $value['url'] . "'>" . $value['name'] . "</a>&nbsp;";
    } ?></div>
    <div id="log_graph"></div>
    <div id="choices"></div>
    <div id="scales">    <b><?php echo lang('graphit:scale') ?></b>
        <?php
        foreach ($scales as $key => $value) {
        echo "<a href='" . $value[0] . "' >" . $key . "</a>|";
    } ?></div>
    {{ pagination:links }}
</div>