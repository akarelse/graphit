<section class="title">
	<h4><?php echo lang('graphit:'.$this->method); ?></h4>
</section>
<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<div class="form_inputs">
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('graphit:name'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('name', set_value('name', $graphit->name), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="slug"><?php echo lang('graphit:slug'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('slug', set_value('slug', $graphit->slug), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="logtime"><?php echo lang('graphit:logtime'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('logtime', set_value('logtime', $graphit->logtime), 'class="width-15" id="datepicker"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="hours"><?php echo lang('graphit:hours'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('hours', set_value('hours', $graphit->hours), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="minutes"><?php echo lang('graphit:minutes'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('minutes', set_value('minutes', $graphit->minutes), 'class="width-15"'); ?></div>
			</li>
			<?php if(!empty($extraitems)): foreach ($extraitems as $key => $value) { ?>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="slug"><?php echo $key; ?></label>
				<div class="input"><?php echo form_input($key, set_value($key, $value), 'class="width-15"'); ?></div>
			</li>
			<?php } endif; ?>
		</ul>
	</div>
	<div class="buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
	</div>
	<?php echo form_close(); ?>
</section>