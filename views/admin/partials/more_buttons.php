<?php if (isset($buttons) && is_array($buttons)): ?>
<?php

	// What type of buttons?
	if(isset($button_type) && $button_type == 'primary'):
		$btn_class = 'btn';
	elseif(isset($button_type) && $button_type == 'secondary'):
		$btn_class = 'button';
	else:
		// Default to primary
		$btn_class = 'btn';
	endif;

?>
<?php foreach ($buttons as $key => $button): ?>
<?php
/**
			* @var		$extra	array associative
	* @since	1.2.0-beta2
*/ ?>
<?php $extra	= NULL; ?>
<?php $button	= ! is_numeric($key) && ($extra = $button) ? $key : $button; ?>
<?php switch ($button) :
	case 'deleteall':
		if($btn_class == 'btn') $btn_class .= ' red';
	
?>
<button type="submit" name="btnAction" value="deleteall" class="<?php echo $btn_class; ?> confirm">
<span><?php echo lang('graphit:delete_all'); ?></span>
</button>
<?php break;

break; ?>
<?php endswitch; ?>
<?php endforeach; ?>
<?php endif; ?>