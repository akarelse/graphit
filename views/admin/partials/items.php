<?php if (!empty($items)): ?>
<table>
	<thead>
		<tr>
			<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
			<th><?php echo lang('graphit:name'); ?></th>
			<th><?php echo lang('graphit:logtime'); ?></th>
			<th></th>
		</tr>
	</thead>
	<tfoot>
	<tr>
		<td colspan="5">
			<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
		</td>
	</tr>
	</tfoot>
	<tbody>
		<?php foreach( $items as $item ): ?>
		<tr>
			<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->logtime; ?></td>
			<td class="actions">
				<?php echo
				anchor('graphit', lang('graphit:view'), 'class="button" target="_blank"').' '.
				anchor('admin/graphit/edit/'.$item->id, lang('graphit:edit'), 'class="button"').' '.
				anchor('admin/graphit/delete/'.$item->id, 	lang('graphit:delete'), array('class'=>'button')); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?php else: ?>
<div class="no_data"><?php echo lang('sample:no_items'); ?></div>
<?php endif;?>