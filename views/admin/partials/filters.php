<fieldset id="filters">
	<legend><?php echo lang('global:filters'); ?></legend>
	<?php echo form_open(''); ?>
	<?php echo form_hidden('f_module', $module_details['slug']); ?>
	<ul>
		<li>
			<?php echo lang('graphit:starttimefilter'); ?>
			<?php echo form_input('f_starttime', '', 'maxlength="10" id="datepicker" class="text width-20"'); ?> &nbsp;
		</li>
		<li>
			<?php echo lang('graphit:stoptimefilter'); ?>
			<?php echo form_input('f_stoptime', '', 'maxlength="10" id="datepicker2" class="text width-20"'); ?> &nbsp;
		</li>
		<li>
			<?php echo lang('graphit:name'); ?>
			<?php echo form_input('name', '', 'maxlength="10" class="text width-20"'); ?> &nbsp;
		</li>
		<li>
			<?php echo anchor(current_url() . '#', lang('graphit:cancel'), 'class="btn gray"') ?>
		</li>
	</ul>
	<?php echo form_close(); ?>
</fieldset>