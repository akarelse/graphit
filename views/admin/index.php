<section class="title">
	<h4>
	<?php echo lang('graphit:item_list'); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php template_partial('filtertemp'); ?>
		<?php echo form_open('admin/graphit/action');?>
		<div id="filter-stage">
			<?php template_partial('itemtemp'); ?>
		</div>
		<div class="table_action_buttons small_bar">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); 
			
			// echo anchor('admin/graphit/deleteall/#', lang('graphit:delete_all'), ' class="confirm btn red"');
			?>
		</div>
		<div class="small_bar">
		<?php $this->load->view('admin/partials/more_buttons', array('buttons' => array('deleteall')));  ?>
		</div>
		<?php echo form_close(); ?>
	</div>
	
</section>