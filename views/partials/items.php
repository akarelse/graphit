<div>
    <div id="pagination">{{ pagination:links }}</div>
    <div id="dataset">{{ items }}</div>
    <div id="scales">
        <b><?php echo lang('graphit:scale') ?></b>
        <?php
        foreach ($scales as $key => $value) {
        echo "<a href='" . $value[0] . "' >" . $key . "</a>|";
        } ?>
    </div>
    <div id="graphnames">
        <b><?php echo lang('graphit:graph_names') ?></b>
        <?php
        foreach ($graphnames as $value) {
        echo "<a href='" . $value['url'] . "'>" . $value['name'] . "</a>&nbsp;";
        } ?>
    </div>
    <div id="averages">
        <b><?php echo lang('graphit:average_of') ?></b>
        <?php
        foreach ($averages as $value) {
        echo "<a href='" . $value['url'] . "'>" . $value['name'] . "</a>&nbsp;";
        } ?>
    </div>
</div>