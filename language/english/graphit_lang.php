<?php
//messages
$lang['graphit:success']		=	'It worked';
$lang['graphit:error']			=	'It didn\'t work';
$lang['graphit:no_items']		=	'No Items';

//page titles
$lang['graphit:create']			=	'Create Item';

//labels
$lang['graphit:name']			=	'Name';
$lang['graphit:slug']			=	'Slug';
$lang['graphit:manage']			=	'Manage';
$lang['graphit:item_list']		=	'Item List';
$lang['graphit:view']			=	'View';
$lang['graphit:edit']			=	'Edit';
$lang['graphit:delete']			=	'Delete';

$lang['graphit:delete_all']		=	'Delete all';
$lang['graphit:graph_names']	=	'Graph names:';
$lang['graphit:average_of']		=	'Average of:';
$lang['graphit:scale']			=	'Scale:';


//buttons
$lang['graphit:custom_button']	=	'Custom Button';
$lang['graphit:items']			=	'Items';

$lang['graphit:starttimefilter']		=	'Between stattime';
$lang['graphit:stoptimefilter']			=	'And stoptime';

?>