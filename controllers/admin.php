<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author 		
 * @website		
 * @package 	
 * @subpackage 
 */
class Admin extends Admin_Controller
{
	protected $section = 'items';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('graphit_m');
		$this->load->library('form_validation');
		$this->lang->load('graphit');

		$this->item_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'logtime',
				'label' => 'Logtime',
				'rules' => 'required'
			),
			array(
				'field' => 'hours',
				'label' => 'hours',
				'rules' => 'required|integer|callback_valid_hour'
			),
			array(
				'field' => 'minutes',
				'label' => 'minutes',
				'rules' => 'required|integer|callback_valid_minute'
			),
			array(
				'field' => 'data',
				'label' => 'Data',
				'rules' => 'trim|max_length[200]'
			)
		);

		$this->template->append_js('module::admin.js')
		->append_css('module::admin.css');
	}

	/**
	 * Callback validation service for minutes
	 * @param int $minute 
	 * @return boolean
	 */
	public function valid_minute($minute)
	{
		if ($minute < 0 || $minute > 60) {
			$this->form_validation->set_message('minute', 'minutes 0 - 60');
			return false;
		}
		return true;
	}
	/**
	  * Callback validation service for hours ?
	 * @param type $hour 
	 * @return boolean
	 */
	public function valid_hour($hour)
	{
		if ($hour < 0 || $hour > 24) {
			$this->form_validation->set_message('hour', 'hours 0 - 24');
			return false;
		}
		return true;
	}

	public function action($id=0)
	{
		// print("koieie");
		// print_r($this->input->post('btnAction'));

		switch ($this->input->post('btnAction'))
		{
			case 'delete':
				$this->delete($id);
			break;
			
			case 'deleteall':
				$this->deleteall();
			break;
			
			default:
				redirect('admin/graphit');
			break;
		}
	}

/**
 * index page controller methingy for index
 * @return void
 */
	public function index()
	{	
		$base_where = array();
		if ($this->input->post('f_starttime')) 	$base_where['starttime'] = $this->input->post('f_starttime');
		if ($this->input->post('f_stoptime')) 	$base_where['stoptime'] = $this->input->post('f_stoptime');
		if ($this->input->post("name")) $base_where['name'] = $this->input->post('name');

		// $total = $this->graphit_m->count_all_names("all");
		$total = $this->pyrocache->model('graphit_m', 'count_all_names',array("all"));

		$pagination = create_pagination("admin/" . $this->module, $total, 100, 3);
		// $items = $this->graphit_m->get_many_by($base_where, $pagination['limit'][1],100);
		$items = $this->pyrocache->model('graphit_m', 'get_many_by',array($base_where, $pagination['limit'][1],100));

		if (!empty($items))
		{

			$temperitems = $items;
			$items = array();
			foreach ($temperitems as $value) 
			{
				$tempitems =  json_decode($value->data);
				unset($tempitems->id);
				unset($tempitems->name);
				unset($tempitems->logtime);
				unset($tempitems->slug);
				unset($value->data);
				$tempitems = (object) array_merge((array)$value,(array)$tempitems);
				$items[] = $tempitems;
			}
		}

		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);

		$this->template
			->title($this->module_details['name'])
			->append_js('admin/filter.js')
			->set_partial('filtertemp', 'admin/partials/filters')
			->set_partial('itemtemp', 'admin/partials/items')
			->set('pagination', $pagination)
			->set('items', $items);

			$this->input->is_ajax_request()
				? $this->template->build('admin/partials/items')
				: $this->template->build('admin/index');
}

	/**
	 * create function for controller to create graph items / points, not realy used
	 * @return void
	 */
	public function create()
	{
		$this->form_validation->set_rules($this->item_validation_rules);
		$_POST['data'] = '{"item":"empty"}';
		if ($this->form_validation->run())
		{

			if ($this->graphit_m->create($this->input->post()))
			{
				$this->pyrocache->delete_all('graphit_m');
				$this->session->set_flashdata('success', lang('graphit.success'));
				redirect('admin/graphit');
			}
			else
			{
				$this->session->set_flashdata('error', lang('graphit.error'));
				redirect('admin/graphit/create');
			}
		}
		
		$graphit = new stdClass;
		foreach ($this->item_validation_rules as $rule)
		{
			$graphit->{$rule['field']} = $this->input->post($rule['field']);
		}

		$this->template
			->title($this->module_details['name'], lang('graphit.new_item'))
			->set('graphit', $graphit)
			->build('admin/form');
	}
	
	/**
	 * edit function for controller to create graph items / points, not realy used
	 * @param type $id 
	 * @return type
	 */
	public function edit($id = 0)
	{
		$graphit = $this->graphit_m->get($id);
		$extraitems = array();

			if (!empty($graphit->data))
			{
				$extraitems =  json_decode($graphit->data);
				unset($extraitems->id);
				unset($extraitems->name);
				unset($extraitems->slug);
			}

		$time = DateTime::createFromFormat('Y-m-d H:i:s',$graphit->logtime);

		$graphit->logtime = $time->format('Y-m-d');
		$graphit->hours = $time->format('H');
		$graphit->minutes = $time->format('i');

		if ($input = $this->input->post())
		{
			$this->form_validation->set_rules($this->item_validation_rules);
			$toencode = $input;
			unset($toencode['name']);
			unset($toencode['hours']);
			unset($toencode['minutes']);
			unset($toencode['id']);
			unset($toencode['slug']);
			unset($toencode['btnAction']);
			$extraitems =  json_encode($toencode);
			$input['data'] = $extraitems;
			$input = array_intersect_key($input, (array) $this->graphit_m->get_names());

			if ($this->form_validation->run())
			{

				unset($_POST['btnAction']);
				if ($this->graphit_m->update($id, $input))
				{
					$this->pyrocache->delete_all('graphit_m');
					$this->session->set_flashdata('success', lang('graphit.success'));
					redirect('admin/graphit');
				}
				else
				{
					$this->session->set_flashdata('error', lang('graphit.error'));
					redirect('admin/graphit/create');
				}
			}
		}

		$this->template
			->title($this->module_details['name'], lang('graphit.edit'))
			->set('graphit', $graphit)
			->set('extraitems', $extraitems)
			->build('admin/form');
	}
	
	public function delete($id = 0)
	{
		$this->pyrocache->delete_all('graphit_m');
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
		{
			$this->graphit_m->delete_many($this->input->post('action_to'));
		}
		elseif (is_numeric($id))
		{
			$this->graphit_m->delete($id);
		}
		redirect('admin/graphit');
	}

	public function deleteall()
	{
		$this->pyrocache->delete_all('graphit_m');
		$this->graphit_m->delete_all();
		redirect('admin/graphit');
	}

}
