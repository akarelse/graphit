<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a graphit module for PyroCMS
 *
 * @author
 * @website
 * @package
 * @subpackage
 */
class Graphit extends Public_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('graphit_m');
        $this->lang->load('graphit');
        
        
        $this->template->append_css('module::graphit.css');
        
        $this->item_validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'logtime',
                'label' => 'Logtime',
                'rules' => 'required'
            ) ,
            array(
                'field' => 'data',
                'label' => 'Data',
                'rules' => 'trim|max_length[200]'
            ) ,
            array(
                'field' => 'newdata',
                'label' => 'newdata',
                'rules' => 'trim'
            )
        );
    }
    
    /**
     * the index page of the graphit module
     * @param string $name
     * @param string $scale
     * @param int $average
     * @param int $offset
     * @return void
     */
    public function index($name = 'all', $scale = 'hourly', $average = 30, $offset = 0) {
        


        $scales = $this->make_scales($name, $average, $offset);
        $scalenum = $scales[$scale][1];
        $averages = $this->make_averages($name, $scale, $offset);

        $total_items = $this->pyrocache->model('graphit_m', 'count_all_names',array($name));
        
        $pagination = create_pagination( $this->module . "/index/" . "/" . $name . "/" . $scale . "/" . $average, $total_items, $scalenum, 6);
        
        // $items = $this->pyrocache->model('graphit_m', 'get_by_scale',array($scalenum, $pagination['limit'][1], $name, $average));
        $items = $this->graphit_m->get_by_scale($scalenum, $pagination['limit'][1], $name, $average);

        $graphnames = $this->pyrocache->model('graphit_m', 'get_graph_names');

        $graphnames = $this->make_graph_names($graphnames , $scale, $average, $offset);
        
        $items_exist = count($items);
        

        if ($this->input->is_ajax_request())
        {
            $this->template->set_layout(FALSE)
            ->set('items', $items)
            ->set('pagination', $pagination)
            ->set('averages', $averages)
            ->set('graphnames', $graphnames)
            ->set('scales', $scales)
            ->build('partials/items');
        }
        else
        {
            $this->template
            ->title($this->module_details['name'])
            ->set('items_exist', $items_exist)
            ->set('pagination', $pagination)
            ->set('graphnames', $graphnames)
            ->set('averages', $averages)
            ->set('scales', $scales)
            ->append_js('module::flot.js')
            ->append_js('module::jquery.flot.resize.js')
            ->append_js('module::jquery.flot.time.js')
            ->append_js('module::graphit.js')
            ->build('index');
        }


    }

    public function getjsonlastlog() 
    {
        print(json_encode($this->graphit_m->get_lastest()));
    }


    
    /**
     * The function to log numeric data and to return json data in the form of commands or status
     * @return void
     */
    public function logit() {
        $datetime = new DateTime();
        $input = $this->input->post();
       
        $input['logtime'] = $datetime->format('Y-m-d H:i:s');
        $this->form_validation->set_rules($this->item_validation_rules);
        $names = Events::trigger('get_all_names_graphit', $input, 'array');
        $names[0]['newdata'] = "newdata";
        $names[0]['dontlog'] = "dontlog";
        $inputa = array_diff_key($input, (array)$names[0]);
        $period = Events::trigger('current_period', array() , 'array');
        $calicmds = Events::trigger('calibrate_commands', array() , 'array');
        $items = array_merge($period, $calicmds);
        
        if (isset($input['newdata']) && count($items) > 0) {
            echo json_encode($items);
        } else {
            print ("OK\n");
        }
        
        $this->form_validation->set_data($input);
        
        

        if (!isset($input['dontlog']) || !isset($calicmds['calibrate'])) {
            if ($this->form_validation->run()) {
                $input['data'] = json_encode(str_replace("'", "\"", $inputa));
                unset($input['id']);
                $this->pyrocache->delete_all('graphit_m');
                Events::trigger('create_real_graph', $input, 'array');
            } else {
                echo "validation errors\n";
                print_r($input);
            }
        } else {
            
            if (isset($calicmds['calibrate'])) {
                print_r($input);
                Events::trigger('calibrate_data_log', $input, 'array');
            } else {
                echo "no calibration done";
            }
        }
    }
    
    private function make_averages($name = "", $scale = 1, $offset = 0) {
        $averages = array(
            5,
            10,
            15,
            20,
            30
        );
        $temp = array();
        foreach ($averages as $value) {
            $temp[] = array(
                'url' => BASE_URL . index_page(). "/" . $this->module . "/index/" . $name . "/" . $scale . "/" . $value . "/" . $offset,
                'name' => $value
            );
        }
        return $temp;
    }
    
    private function make_scales($name = "", $average = 5, $offset = 0) {
        $scales = array(
            "quarterly" => 15,
            "halfly" => 30,
            "hourly" => 60,
            "dayly" => 1440,
            "weekly" => 10080
        );
        $temp = array();
        foreach ($scales as $key => $value) {
            $temp[$key][0] = BASE_URL . index_page(). "/" . $this->module . "/index/" . $name . "/" . $key . "/" . $average . "/" . $offset;
            $temp[$key][1] = $value;
        }
        return $temp;
    }
    
    private function make_graph_names($graphnames, $scale = 1, $average = 5, $offset = 0) {
        $temp = array();
        
        foreach ($graphnames as $value) {
            $temp[] = array(
                'url' => BASE_URL . index_page(). "/" . $this->module . "/index/" . $value->name . "/" . $scale . "/" . $average . "/" . $offset,
                'name' => $value->name
            );
        }
        return $temp;
    }
    
    private function make_graph_json($items) {
        


        if (count($items) > 1) {
            $temperitems = $items;
            $items = array();
            


            $index = 0;
            foreach ($temperitems as $value) {
                $tempitems = json_decode($value->data);
                $temptime = DateTime::createFromFormat('Y-m-d H:i:s', $value->logtime);
                unset($tempitems->time);
                
                foreach ($tempitems as $key => $value) {
                    $items[$key][] = array(
                        $temptime->format('U') ,
                        $value
                    );
                }
            }


            
            foreach ($items as $key => $value) {
                $items[$index]['data'] = $value;
                $items[$index]['label'] = $key;
                unset($items[$key]);
                $index++;
            }
            
            

            $items = json_encode($items);
            $items = preg_replace("/\"(\d+)\"/", '$1', $items);
        }
        return $items;
    }

    // public function seed()
    // {
    //     $this->faker = Faker\Factory::create();
    //     $datetime = new DateTime();
    //     $data = json_encode(array('label' => 'testvalue','data' => $this->faker->numberBetween(0, 2)));
    //     $input = array(
    //     'dontlog' => false,
    //     'logtime' => $datetime->format('Y-m-d H:i:s'),
    //     'name'=>'seedtest',
    //     'slug'=>'seedtest',
    //     'data'=> $data
    //     );

    //     $this->graphit_m->create_real($input);
    // }
}
