<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a graphit module for PyroCMS
 *
 * @author
 * @website
 * @package
 * @subpackage
 */
class Graphit_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->_table = 'graphit';
    }
    
    public function create($input) {
        if (isset($input['logtime'])) {
            $input['logtime'] = $input['logtime'] . " " . $input['hours'] . ":" . $input['minutes'] . ":00";
        }
        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'data' => $input['data'],
            'logtime' => $input['logtime']
        );
        return $this->db->insert('graphit', $to_insert);
    }
    
    public function _check_slug($slug) {
        $slug = strtolower($slug);
        $slug = preg_replace('/\s+/', '-', $slug);
        return $slug;
    }
    
    public function get_graph_names() {
        $results = $this->db->select('name')->distinct()->get($this->_table)->result();
        $results[] = (object)array(
            "name" => "all"
        );
        return $results;
    }
    
    public function count_all_names($name = 'all') {
        if ($name == 'all') {
            $this->db->from($this->_table);
        } else {
            $this->db->where('name', $name)->from($this->_table);
        }
        return $this->db->count_all_results();
    }
    
    public function get_by_scale($scale = 0, $offset = 0, $name = '', $reduceto) {
        if ($name != 'all') {
            $this->db->where('name', $name);
        }
        
        $this->db->order_by("logtime", "asc");
        $results = $this->db->get($this->_table, $scale, $offset)->result();
        
        $results = $this->splitout($results);
        // $results = $this->other_average($results, $reduceto);
        // print_r($results);
        $results = preg_replace("/\"(\d+)\"/", '$1', json_encode($results));
        // print_r($results);
        return $results;
    }

    public function other_average($results, $reduceto)
    {
       foreach ($results as $key => $value) {
           foreach ($variable as $key => $value) {
               # code...
           }
       }
    }
    
    public function average_off($results, $reduceto) {
        if ($reduceto == 0) {
            return $results;
        }
        $tempar1 = array();
        $som = 0;
        $timesom = 0;
        $i = 1;
        foreach ($results as $key => $value) {
            $tempar2 = array();
            foreach ($value as $k => $v) {
                $tempar3 = array();
                if (is_array($v) && $k == 'data') {
                    $divnum = round(count($v) / $reduceto);
                    foreach ($v as $ke => $val) {
                        $som = + $val[1];
                        if ($i >= $divnum && $divnum > 0) {
                            $i = 0;
                            $tempar3[] = array(
                                $val[0],
                                $som / $divnum
                            );
                            
                            // $tempar3[$ke][] =
                            $som = 0;
                            $timesom = 0;
                        }
                        $i++;
                    }
                    $tempar2[$k] = $tempar3;
                } elseif ($k == 'label') {
                    $tempar2[$k] = $v;
                }
            }
            $tempar1[$key] = $tempar2;
        }
        return $tempar1;
    }
    
    public function get_lastest() {
        $results = $this->db->order_by("logtime", "desc")->get($this->_table, 1, 0)->result();
        unset($results->id);
        return (array)json_decode($results[0]->data);
    }
    
    public function get_data_names_by_name($name) {
        $this->db->select('data')->from($this->_table)->where('name =', $name['name'])->distinct();
        $tempar = array();
        $results = $this->db->get()->result();
        foreach ($results as $value) {
            $dataar = (array)json_decode($value->data);
            $tempar = array_merge(array_keys($dataar) , $tempar);
        }
        $tempar = array_unique($tempar);
        return $tempar;
    }
    
    public function splitout($items, $asjson = true) {
        
        $temperitems = $items;
        $items = array();
        // $extra =  array();
        $labels = array();
        $i2 = 0;
        // print_r($temperitems);
        foreach ($temperitems as $v) {
            $tempitems = json_decode($v->data);
            $temptime = DateTime::createFromFormat('Y-m-d H:i:s', $v->logtime);
            
            foreach (array_keys(get_object_vars($tempitems)) as $k)
            {
                $items[$k]['label'] = $k;

                $items[$k]['data'][] =  array((float)$temptime->format('U')*1000, (float)$tempitems->$k);

            }
            
            // print_r($tempitems);
        
            // print_r($tempitems);
            // unset($tempitems->time);

            // $items['label'] = $tempitems->label;
            // $items['data'][ (float)$temptime->format('U') * 1000] = $tempitems->data;
               
            
        }
      
        // print_r(json_encode(array($items)));
        return $items;
    }
    
    public function create_real($input) {
        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'data' => $input['data'],
            'logtime' => $input['logtime']
        );
        return $this->db->insert('graphit', $to_insert);
    }
    
    public function get_names() {
        $fields = $this->db->field_data($this->_table);
        $names = array();
        foreach ($fields as $field) {
            $names[$field->name] = $field->name;
        }
        return $names;
    }
    
    public function get_many_by($params = array() , $offset = 0, $scale = 100) {
        $this->db->select('*');
        if (!empty($params['name'])) {
            $this->db->like('name', $params['name']);
        }
        if (!empty($params['starttime']) && !empty($params['stoptime'])) {
            $datetime1 = new DateTime($params['starttime']);
            $datetime2 = new DateTime($params['stoptime']);
            if ($datetime1 < $datetime2) {
                $this->db->where("logtime > '" . $params['starttime'] . "' AND logtime < '" . $params['stoptime'] . "'", NULL, FALSE);
            }
        } elseif (!empty($params['starttime']) && empty($params['stoptime'])) {
            $this->db->where('logtime >', $params['starttime']);
        } elseif (!empty($params['stoptime']) && empty($params['starttime'])) {
            $this->db->where('logtime <', $params['stoptime']);
        }
        return $this->db->get($this->_table, $scale, $offset)->result();
    }
    
    public function delete_all() {
        $this->db->empty_table($this->_table);
    }
}
