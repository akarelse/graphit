document.addEventListener("DOMContentLoaded", function(event) { 
jQuery(function($) {
    $("#datepicker2").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    pyro.generate_slug('input[name="name"]', 'input[name="slug"]');
    filter_form = $('#filters form');
    filter_form.find('#datepicker').on('change', function() {
        form_data = pyro.filter.$filter_form.serialize();
        pyro.filter.do_filter(pyro.filter.f_module, form_data);
    });
    filter_form.find('#datepicker2').on('change', function() {
        form_data = pyro.filter.$filter_form.serialize();
        pyro.filter.do_filter(pyro.filter.f_module, form_data);
    });


});
});