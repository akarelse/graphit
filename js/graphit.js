document.addEventListener("DOMContentLoaded", function(event) { 

    var choiceContainer = $("#choices");
    getNewdata(window.location.href);

    choiceContainer.click(function() {
        // console.log($.plot.getData());
        // getNewdata();
    });

    function getNewdata(url) {
        if (url == undefined) {
            url='';
        }
        // console.log();
        // console.log("++"+url+"++");
        $.get(url, function(data) {
            if(data != undefined)
            {
                html = $(notWorking(data));
                dataset = $("#dataset", html);

                pagination = $("#pagination", html);
                averages = $("#averages", html);
                scales = $("#scales", html);
                graphnames = $("#graphnames", html);
                dataset = $.parseJSON(dataset.text());
                plotAccordingToChoices(dataset);

                $(".pagination").replaceWith(pagination);
                $("#scales").replaceWith(scales);
                $("#averages").replaceWith(averages);
                $("#graphnames").replaceWith(graphnames);
                rebuildlinks();
            }

        });

    }

    function plotAccordingToChoices(data) {
        $.each(data, function(key, val) {
            // console.log(val)
            if (val && ("label" in val) && (choiceContainer.find("#id" + val.label).length == 0)) {
                choiceContainer.append("<input type='checkbox' name='" + val.label + "' checked='checked' id='id" + val.label + "'></input>" + "<label for='id" + val.label + "'>" + val.label + "</label>");
            }
        });

        var temp = [];
        choiceContainer.find("input:checked").each(function() {
            var key = $(this).attr("name");
            if (key) {
                $.each(data, function(keyx, value) {
                    if (key == value['label']) {
                        temp.push(value);
                    }
                });
            }
        });

        if (temp.length > 0) {
            data = temp;
        }

        // console.log(data);

        if (data.length > 0) {
            $.plot("#log_graph", data, {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timezone: "browser",
                    timeformat: "%H:%I"
                },
                grid: {
                    hoverable: true,
                    clickable: true
                }
            });
        }
    }


    function rebuildlinks() {
        $(".graphit-container a").each(function() {
            $(this).on("click", function(e) {
                e.preventDefault();
                getNewdata($(this).attr('href'));

            });
        })
    }

    var notWorking = function(html) {
        var el = document.createElement('div');
        el.innerHTML = html;
        return el.childNodes[0];
    }


});