<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Graphit extends Module {

	public $version = '2.1';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Graphit'
			),
			'description' => array(
				'en' => 'This is a PyroCMS module graphit.'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content', // You can also place modules in their top level menu. For example try: 'menu' => 'Graphit',
			'sections' => array(
				'items' => array(
					'name' 	=> 'graphit:items', // These are translated from your language file
					'uri' 	=> 'admin/graphit',
					'shortcuts' => array(
                        // 'create' => array(
                        //     'name' => 'graphit:create',
                        //     'uri' => 'admin/graphit/create',
                        //     'class' => 'add'
                        // 	)
                    	)
					)
				)
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('graphit');
		$this->db->delete('settings', array('module' => 'graphit'));

		$graphit = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'logtime' => array(
				'type' => 'DATETIME',
				'null' => false,
				'default' => '0001-01-01 01:01:01'
			),
			'data' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'default' => '{"item":"empty"}'
			)
		);

		$graphit_setting = array(
			'slug' => 'update intervall',
			'title' => 'Update Interval',
			'description' => 'The interval of logging',
			'`default`' => '10',
			'`value`' => '10',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'graphit'
		);



		$this->dbforge->add_field($graphit);
		$this->dbforge->add_key('id', TRUE);
		if($this->dbforge->create_table('graphit') AND $this->db->insert('settings', $graphit_setting))
		{
			return TRUE;
		}
	}

	public function uninstall()
	{
		$this->db->delete('settings', array('module' => 'graphit'));
		$this->dbforge->drop_table('graphit');
		{
			return TRUE;
		}
	}

	public function upgrade($old_version)
	{
		return TRUE;
	}

	public function help()
	{
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
